from flask import Flask
application = Flask(__name__)

@application.route('/')
def hello_world():
    return ('Hello-world, this is me. Fourth update via CLI')


if __name__ == '__main__':
    application.run()